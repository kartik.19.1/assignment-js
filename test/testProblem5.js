const problem5 = require('../problem5');
const problem4 = require('../problem4');
const data = require('../data');


let carYears = problem4(data.inventory);
let year = 2000;
let cars = problem5(carYears, year);
console.log(cars.length);  

year = 2007;
cars = problem5(carYears, year);
console.log(cars.length); 

year = '2007';
cars = problem5(carYears, year);
console.log(cars);

year = 2007;
cars = problem5(undefined, year);
console.log(cars);

cars = problem5(carYears);
console.log(cars);

carYears = problem4('data.inventory');
cars = problem5(carYears, year);
console.log(cars);


