const problem3 = require('../problem3');
const data = require('../data');


let cars = problem3(data.inventory);
for(let i = 0; i < cars.length; i++){
  console.log(cars[i].car_model);
}

cars = problem3();
console.log(cars);

cars = problem3([]);
console.log(cars);

cars = problem3('hello');
console.log(cars);

cars = problem3(1234);
console.log(cars);