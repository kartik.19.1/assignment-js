const problem4 = require('../problem4');
const data = require('../data');


let carYears = problem4(data.inventory);
for(let i = 0; i < carYears.length; i++){
  console.log(carYears[i]);
}

carYears = problem4('hello World');
console.log(carYears);

carYears = problem4(1234);
console.log(carYears);

carYears = problem4(undefined);
console.log(carYears);

carYears = problem4();
console.log(carYears);