function compareFn(first, second){
  if(first.car_model.toLowerCase() === second.car_model.toLowerCase()) {
    return 0;
  }
  else if(first.car_model.toLowerCase() < second.car_model.toLowerCase()) {
    return -1;
  }
  else {
    return 1;
  }
}

function sortCarModels(inventory){
  if(!Array.isArray(inventory)){
    return [];
  }
  inventory.sort(compareFn);
  return inventory;
}

module.exports = sortCarModels;
