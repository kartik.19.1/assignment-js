function allCarYears(inventory){
  if(!Array.isArray(inventory)){
    return [];
  }
  let carYears = new Set();
  for(let i = 0; i < inventory.length; i++){
    carYears.add(inventory[i].car_year);
  }
  return Array.from(carYears);
}

module.exports = allCarYears;
