function lastCar(inventory){
  if(!Array.isArray(inventory) || inventory.length === 0){
    return [];
  }
  return inventory[inventory.length - 1];
}

module.exports = lastCar;
