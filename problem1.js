function getCarById(inventory, id){
  if(!Array.isArray(inventory) || id === undefined){
    return [];
  }
  for(let i = 0; i < inventory.length; i++){
    if(inventory[i].id === id){
     return inventory[i];
    }
  }
  return [];
}

module.exports = getCarById ;
