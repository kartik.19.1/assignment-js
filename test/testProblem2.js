const problem2 = require('../problem2');
const data = require('../data');

let car = problem2(data.inventory);
console.log(`Last car is a ${car.car_make} ${car.car_model}`);

car = problem2('hello');
console.log(car);

car = problem2(1234);
console.log(car);

car = problem2([]);
console.log(car);

car = problem2(undefined);
console.log(car);

car = problem2(null);
console.log(car);